<!DOCTYPE html>
<html>
	<head>
		@yield('headContents')

	</head>
	<body>
		@yield('bodyContents')
	</body>
</html>